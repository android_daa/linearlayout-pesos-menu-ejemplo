package com.example.duoc.fragmentsdos;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.duoc.fragmentsdos.fragments.DetalleFragment;


public class MainActivity extends ActionBarActivity {

    private Button btnHola;
    private Button btnVolver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnHola = (Button)findViewById(R.id.btnHola);
        btnHola.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cambiarFragmentDos();
            }
        });

        btnVolver = (Button)findViewById(R.id.btnVolver);
        btnVolver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                volverFragment();
            }
        });

    }

    private void alert(String mensaje){
        Toast.makeText(this,mensaje, Toast.LENGTH_LONG).show();
    }

    private void cambiarFragmentDos(){
        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.content, new DetalleFragment(), "hola");
        ft.addToBackStack(null);
        ft.commit();
    }

    private void volverFragment(){
        getFragmentManager().popBackStackImmediate();
    }




}
